# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# determine what gear is needed based on the day and weather
def gear_for_day(is_workday, is_sunny):

    # creates list of gear available
    # available = ["laptop", "surfboard", "umbrella"]
    # creates list of gear required
    # required = []
    rainy_workday_gear = ["laptop", "umbrella"]
    sunny_workday_gear = ["laptop"]
    rainy_offday_gear = ["umbrella", "surfboard"]
    sunny_offday_gear = ["surfboard"]

    # if sunny and a workday
    if is_sunny and is_workday:
        # pack laptop and surfboard
        # required.append(available[0])
        # required.append(available[1])
        return sunny_workday_gear
    #if not sunny and a workday
    if is_sunny == False and is_workday:
        #pack laptop and umbrella
        # required.append(available[2])
        # required.append(available[0])
        return rainy_workday_gear
    #if sunny and a nonworkday
    if is_sunny and is_workday == False:
        #pack surfboard
        # required.append(available[1])
        return sunny_offday_gear
    #if not sunny and a nonworkday
    if is_sunny == False and is_workday == False:
        #pack umbrella and surfboard
        # required.append(available[2])
        # required.append(available[1])
        return rainy_offday_gear

    # return required

workday = False
sunnyday = True
print(gear_for_day(workday, sunnyday))
