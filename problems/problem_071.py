# Write a class that meets these requirements.
#
# Name:       Employee
#create class for Employee
class Employee:
    #create method for init with first and last name states
    def __init__(self, first, last):
        #make self.first equal to first
        self.first = first
        #make self.last equal to last
        self.last = last
#
# Required state:
#    * first name, a string
#    * last name, a string
#
# Behavior:
#    * get_fullname: should return "«first name» «last name»"
#    * get_email:    should return "«first name».«last name»@company.com"
#                    all in lowercase letters
    #create functions  to get_fullname
    def get_fullname(self):
        #add first name to last name
        return (f"{self.first} {self.last}")
    #create function to get_email:
    def get_email(self):
        #add first and last to get email
        email = (self.first + "." + self.last + "@company.com")
        return email.lower()

# Example:
employee = Employee("Duska", "Ruzicka")

print(employee.get_fullname())  # prints "Duska Ruzicka"
print(employee.get_email())     # prints "duska.ruzicka@company.com"
#
# You may want to look at the ".lower()" method for strings to
# help with this code.
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
