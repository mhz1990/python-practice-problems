# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

#determine whether x and y values are within the bounds of the rectangle
def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    #if x is greater than or equal to rect_x and less than or equal to rect_x + rect_width and y is
    #  greater than or equal to rect_y and y is less than or equal to rect_y + rect_height
    if x >= rect_x and x <= rect_x + rect_width and y >= rect_y and y <= rect_y + rect_height:
        #return true
        return True
    #otherwise
    else:
        #return false
        return False

x = 3
y = 2
rect_x = 0
rect_y = 0
rect_width = 4
rect_height = 4
print(is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height))
# print(is_inside_bounds(5, 5, 2, 3, 4, 4)) # True
# print(is_inside_bounds(0, 0, -1, 4, 4, 4)) # False
