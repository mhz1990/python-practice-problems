# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

#determine the average of the values in the input list
def calculate_average(values):

    #if the list is empty
    if len(values) == 0:
        #return "None"
        print("None")
        return "None"

    #create sum variable
    sum = 0

    #iterate over input list
    for current in values:
        #push current value squared to new list
        sum += current

    #create average variable
    average = sum / len(values)
    print(average)

numbers = [1, 2, 3, 4, 5, 6, 7]
calculate_average(numbers)
