# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# should remove all duplicates from input string
def remove_duplicate_letters(s):

    # if input string is empty

    # create result string
    result = ""

    # iterate over input string
    for current in s:
        # if current character does not exist in
        if current not in result:
            # continue
            result += current
        # else:
        #     result += current

    # return result string
    print(result)
    return result

letters = "mmmaaahhha"
remove_duplicate_letters(letters)
