# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#minimum value compares the input values
def minimum_value(value1, value2):
    #if value1 is greater than or equal to value2
    if value1 <= value2:
        #return value1
        return value1
    #otherwise
    else:
        #return value2
        return value2
    print(minimum_value)
    # #returns the minimum value
    # return minimum_value

values1 = 1
values2 = 2
print(minimum_value(values1, values2))
