# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

# determine average grade
def calculate_grade(values):

    # create variable for sum
    sum = 0

    # iterate over input list
    for current in values:
        #make sum equal to sum plus current
        sum += current
        # print(sum)
    # create variable for average
    average = round(sum / len(values))
    print(average)

    # if average of grades is below 60
    if average < 60:
        #return "F"
        return "F"
    # if average is below 70
    if average < 70:
        #return "D"
        return "D"
    # if average is below 80
    if average < 80:
        #return "C"
        return "C"
    # if average is below 90
    if average < 90:
        #return "B"
        # print("B")
        return "B"
    # if average is below or equal to 100
    if average <= 100:
        #return "A"
        return "A"

grades = [90, 80, 95, 98, 81, 78, 86]
calculate_grade(grades)
