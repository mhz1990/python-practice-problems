# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    #make lenght into string
    len_string = "*" * length
    print(len_string)

    #make input number into string
    num_string = str(number)
    print(num_string)

    #iterate over length value
    for i in len_string:
        #if length of num string is less than length
        if len(num_string) < length:
            #num string equals pas string plus num string
            num_string = pad + num_string
    #print num string
    print(num_string)
    #return num string
    return num_string


pad_left(55, 8, " ")
