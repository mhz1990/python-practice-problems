# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# determine whether number is divisible by 3
def is_divisible_by_3(number):

    if type(number) != int:
        return("Not a number")

    #if number is divisible by 3
    if number % 3 == 0:
        #return the word "fizz"
        return "fizz"
    #otherwise
    else:
        #return the input number
        return number

num = 3
print(is_divisible_by_3(num))
