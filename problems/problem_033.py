# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None

    #create sum_evens variable of 0
    sums = 0
    #for i in range of n:
    for i in range(n+1):
        #if the remainder of i divided by two is 0
        if i % 2 == 0:
            #make sum equal to sum plus i
            sums += i
    #print then return sum
    print(sums)
    return sums

sum_of_first_n_even_numbers(10)
