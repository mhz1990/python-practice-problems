# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#determine whether input number is divisible by 5
def is_divisible_by_5(number):

    if type(number) != int:
        print("input not valid")
    #if number is divisible by 5 with no remainder
    elif number % 5 == 0:
        #return the word "buzz"
        print("buzz")
    #otherwise
    else:
        #return the number
        print(number)

num = 10.2
is_divisible_by_5(num)
