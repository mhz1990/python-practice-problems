# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

# determine maximum value in list
def max_in_list(values):
    #if input list is empty
    if len(values) == 0:
        #return "None"
        return "None"

    #create variable for maximum value
    maximum = 0

    #iterate over inputt list
    for i in values:
        #if current is greater than maximum
        if i > maximum:
            #make maximum equal current
            maximum = i

    print(maximum)
    #return maximum
    return maximum

numbers = [90, 80, 95, 98, 81, 78, 86]
max_in_list(numbers)
