# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#determine the maximum of three input values
def max_of_three(value1, value2, value3):
    #if value 1 is greater than values 2 and 3
    if value1 >= value2 and value1 >= value3:
        #return value 1
        return value1
    #otherwise if value 2 is greater than values 1 and 3
    elif value2 >= value1 and value2 >= value3:
        #return value 2
        return value2
    #otherwise
    else:
        # return value3
        return value3

val1 = 1
val2 = 2
val3 = 3
# max_of_three(1, 2, 3)
print(max_of_three(val1, val2, val3))
