# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#determine if I can make pasta
def can_make_pasta(ingredients):

    #make list of required ingredients
    requirements = ["flour", "eggs", "oil"]

    #if ingredients list does not contain 3 items
    if len(ingredients) != len(requirements):
        #return "ingredients incorrect"
        return "incorrect ingredients"

    for ingredients in requirements:
        #if ingredients list contains flour, eggs, and oil
        if ingredients in requirements:
            #return true
            return True
        #otherwise
        else:
            return False

available_ingredients = ["flour", "eggs", "oil"]
print(can_make_pasta(available_ingredients))
