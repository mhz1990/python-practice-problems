# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#determine sum of averages of input list
def sum_of_first_n_numbers(limit):

    # if value of limit is less than 0
    if limit < 0:
    #     #return None
        print(None)
        return None

    # #created nums list variable from range

    #create sums variable equal to 0
    sums = 0

    #iterate over input range
    for i in range(limit + 1):
        #append current to numbers list
        # nums_list.append(i)
        #add sums plus current nums list at current value
        sums += i
        print(sums)

    #return the sum of averaged inputs
    print(sums)
    return sums

sum_of_first_n_numbers(5)
