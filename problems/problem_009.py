# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#determines if input word is palindrome
def is_palindrome(word):

    reversed_word = reversed(word)
    # print(reversed(word1))
    #if the input word is the same as the input word backwards
    if word == "".join(reversed_word):
        #return true
        print(word + " is palindrome!")
    else:
        print(word + " is not palindrome!")

word1 = is_palindrome("racecar")

# reversed = word1[::-1]
# print(is_palindrome(word1))

# txt = "Hello World"[::-1]
# print(txt)
