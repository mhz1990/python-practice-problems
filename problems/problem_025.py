# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
#determine sum of input values
def calculate_sum(values):

    #if input number list is empty
    if len(values) == 0:
        #return "None"
        print("None")
        return "None"

    #create new list for sum
    sum = 0

    #iterate over input list
    for current in values:
        #add current to new list sum
        sum += current
    #return new list
    print(sum)
    return sum
numbers = [1, 2, 3, 4, 5, 6, 7]
calculate_sum(numbers)
