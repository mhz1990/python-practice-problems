# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#determine second largest number in input list
def find_second_largest(values):

    # #if input list is empty
    # if len(values) == 0:
    #     #return None
    #     return None

    # #if len(values) == 1:
    # if len(values) == 1:
    #     #return None
    #     return None

    #sort numbers in input list
    values_sorted = sorted(values)
    #return item at -2 index in list
    return(values_sorted[-2])
    # return second_largest

numbers = [3, 1, 5]
num_list = find_second_largest(numbers)
