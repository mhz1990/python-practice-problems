# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2


def count_letters_and_digits(s):
    #create two variables, is alph and is num set equal to 0
    is_alph = 0
    is_num = 0
    #iterate over input string
    for i in s:
        #if current is alph
        if i.isalpha() == True:
            #make is alph equal to is alph plus 1
            is_alph += 1
            print(is_alph)

        #else if current in num
        elif i.isdigit() == True:
            #make is num equal to is alph plus 1
            is_num += 1
            print(is_num)

    #print is alph and is num
    print(is_alph, is_num)
    #return is alph and is num
    return(is_alph, is_num)

count_letters_and_digits("Th12 12 a te2t")
